using System.IO;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Media.Imaging;
using NAudio.Wave;

namespace Lab4S4.Views;

public partial class MainWindow : Window
{
    private WaveOutEvent _outputDevice;
        private AudioFileReader _audioFile;

        public MainWindow()
        {
            InitializeComponent();
            this.KeyDown += Form_KeyDown;

            _audioFile = new AudioFileReader(Path.Combine(Directory.GetCurrentDirectory(),
                "Lab4S4/Assets/Som.mp3")); // Caso não funciona no seu coloque o endereço absoluto
            _outputDevice = new WaveOutEvent();
            _outputDevice.Init(_audioFile);
        }

        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            var margin = foquete.Margin;

            if (e.Key == Key.Up)
            {
                margin = new Thickness(margin.Left, margin.Top - 5, margin.Right, margin.Bottom);
                _outputDevice.Play();
            }
            else if (e.Key == Key.Down)
            {
                margin = new Thickness(margin.Left, margin.Top + 5, margin.Right, margin.Bottom);
            }

            foquete.Margin = margin;
        }
}